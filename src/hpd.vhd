-- * Delay substract
--
--! @file
--!
--! @brief Input input averaging over a _g_window_ length window
--!
--! Performs an averaging of an arbitrary size input input stream over a time
--! windows of size _2**g_window_, being _g_window_ a generic input to the module.
--!
--! @section avg Averaging
--!
--! Averaging is implemented as the difference of the registered input with its
--! _2**g_window_ shifted version. Result is continuously accumulated to compute a
--! (_2**g_window_ sized) moving window over the input.
--!
--! @section div Division
--!
--! Its output is left bit shifted by _g_window_ to compute division by _2**g_window_.
--!
--! @class averager

-- * Libraries

library ieee;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

-- * Entity

--! @brief Gets g_window as a generic to compute a 2**g_window length window.
--!
--! @details Rst and clk have standard functionality\n
--! Samples is an arbitrary number of bites input data bus\n\n
--! Average is the sum of all input in the window, divided by its length,
--! thus the average\n
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...
--!
entity hpd is
  generic (g_m1  : integer;
           g_m2  : integer;
           g_acc : natural := 10);
  port (
    --!\name Global signals
    --!\{
    clk    : in  std_logic;
    rst    : in  std_logic;
    --!\}
    --!\name Input and averaged input
    --!\{
    input  : in  signed;
    --! length is 2*input'length+g_acc
    output : out signed
   --!\}
    );
end entity hpd;

-- * Architecture

--! @brief Implements a simple, general purpose moving averaging.
--!
--! @details It delays the incoming sampling signals, sampling them in parallel.\n
--! Then, takes the difference of the two parallel streams\n
--! accumulating the difference.\n
--! Finally, redimensions the results to compute the division by the window lenght
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...
--!
architecture modular of hpd is

  -- ** signals
  --
  --! Registered incoming input
  signal input_m1, input_m2 : signed(2*input'length-1 downto 0) := (others => '0');

  --! Shifted incoming input
  signal input_m2_acc : signed(input_m2'length+g_acc-1 downto 0) := (others => '0');

  signal add : signed(2*input'length+g_acc-1 downto 0) := (others => '0');

begin

  -- ** register input

  --! @brief Register process
  --!
  --! @details Just register incoming input\n
  --! The register size it’s been fixed during signal declaration\n
  --! Note that it performs signed arithmetic.
  --!
  --! @section sec1 Section1
  --!
  --! All is clear, I guess ...
  --!
  --! @section sec2 Section2
  --!
  --! Don’t know what to say ...

  p_mw_register_input : process (clk) is
  begin
    if rising_edge(clk) then
      if rst = '1' then
        input_m1 <= (others => '0');
      else
        input_m1 <= input*g_m1;
      end if;
    end if;
  end process;

  -- ** shift input by 2**g_window=c_window_length clock periods

  --! @brief Shift register process
  --!
  --! @details Sum up the result of the difference block\n
  --! The register size it’s been fixed during signal declaration\n
  --! Note that it performs signed arithmetic.
  --!
  --! @section sec1 Section1
  --!
  --! All is clear, I guess ...
  --!
  --! @section sec2 Section2
  --!
  --! Don’t know what to say ...

  input_m2 <= input*g_m2;

  p_mw_shift_register : process (clk) is
  begin
    if rising_edge(clk) then
      if rst = '1' then
        input_m2_acc <= (others => '0');
      else
        -- shift register output
        input_m2_acc <= input_m2_acc + input_m2;
      end if;
    end if;
  end process;

  -- ** dsflkjfds

  p_shift_register : process (clk) is
  begin
    if rising_edge(clk) then
      if rst = '1' then
        add <= (others => '0');
      else
        add <= input_m1 + input_m2_acc;
      end if;
    end if;
  end process;

  --! output size is fixed, that of add
  output <= add;

end architecture modular;
